import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wild_life/screens/ActsAndRouls.dart';
import 'package:wild_life/screens/LicensesPage.dart';
import 'package:wild_life/screens/PeasantryPage.dart';
import 'package:wild_life/screens/ProtectedAreas.dart';
import 'package:wild_life/screens/Survey.dart';
import 'package:wild_life/screens/loginpage.dart';

import 'DepredationRoadKills.dart';
import 'OffencesPage.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    var cardTextStyle = TextStyle(
        fontFamily: 'Montserrat Regular',
        fontSize: 16,
        color: Color.fromRGBO(63, 63, 63, 1));

    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: size.height * .3,
            decoration: BoxDecoration(
                image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/images/top_header.png'),
              fit: BoxFit.fill,
            )),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Container(
                    height: 64,
                    margin: EdgeInsets.only(
                      bottom: 20,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/user.jpeg'),
                          radius: 32,
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Mr. Wild Life',
                              style: TextStyle(
                                  fontFamily: 'Montserrat Medium',
                                  color: Colors.black,
                                  fontSize: 20),
                            ),
                            Text(
                              'Co-Founder Wild Life',
                              style: TextStyle(
                                  fontFamily: 'Montserrat Medium',
                                  color: Colors.black,
                                  fontSize: 14),
                            ),
                          ],
                        ),
                        Expanded(
                          child: PopupMenuButton(
                            itemBuilder: (context) => [
                              PopupMenuItem(
                                child: Text("English"),
                                value: 1,
                              ),
                              PopupMenuItem(
                                child: Text("Urdu"),
                                value: 2,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  //Grid View
                  Expanded(
                    child: GridView.count(
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      primary: false,
                      crossAxisCount: 2,
                      children: [
                        Card(
                          color: Colors.cyanAccent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 4,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ActsAndRuls()));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/graduated.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'first',
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          color: Colors.cyanAccent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 4,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SurveyPage()));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/books.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "second",
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          color: Colors.cyanAccent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 4,
                          child: InkWell(
                            onTap: () {
                              // context.locale = Locale('es', 'US');
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ProtectedAreasPage()));
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/schedule.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'third',
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          color: Colors.cyanAccent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 4,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PeasantryPage()));
                              // context.locale = Locale('en', 'US');
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/online-course.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'fourth',
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LicensesPage()));
                          },
                          child: Card(
                            color: Colors.cyanAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/studying.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'five',
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => OffencesPage()));
                          },
                          child: Card(
                            color: Colors.cyanAccent,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/pencil.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'six',
                                  style: cardTextStyle,
                                ).tr(),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => DepredationRoadKills()));
                          },
                          child: Card(
                            color: Colors.cyanAccent,
                            // color: Color((Random().nextDouble() * 0xFFFFFF)
                            //             .toInt() <<
                            //         0)
                            //     .withOpacity(1.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            elevation: 4,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/graduated.svg',
                                  height: 90,
                                  width: 90,
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  'Depredation &\n Road Kills',
                                  textAlign: TextAlign.center,
                                  style: cardTextStyle,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
