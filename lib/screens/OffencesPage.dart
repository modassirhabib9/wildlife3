import 'package:flutter/material.dart';
import 'package:wild_life/screens/dashboard.dart';

class OffencesPage extends StatefulWidget {
  const OffencesPage({Key? key}) : super(key: key);

  @override
  _OffencesPageState createState() => _OffencesPageState();
}

class _OffencesPageState extends State<OffencesPage> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Offences"),
        backgroundColor: Colors.lightBlue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: "Offence ID",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                // SizedBox(height: 10),
                // Container(
                //   height: 52,
                //   child: TextField(
                //     obscureText: obscureText,
                //     decoration: InputDecoration(
                //       suffixIcon: GestureDetector(
                //         onTap: () {
                //           setState(() {
                //             obscureText = !obscureText;
                //           });
                //         },
                //         child: Icon(
                //           obscureText == true
                //               ? Icons.visibility_outlined
                //               : Icons.visibility_off_outlined,
                //         ),
                //       ),
                //       labelText: 'Password',
                //       border: OutlineInputBorder(
                //           borderRadius: BorderRadius.circular(10),
                //           borderSide: BorderSide(color: Color(0xFFAAABB0))),
                //       // prefixIcon: Icon(Icons.lock),
                //     ),
                //   ),
                // ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Division',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'District',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Name',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),

                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Address',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),

                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Offence Type',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Offence Nature',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Date',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Name of Court ',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Amount of Court ',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 52,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Remarks',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color(0xFFAAABB0),
                        ),
                      ),
                      // prefixIcon: Icon(Icons.account_box_sharp),
                    ),
                  ),
                ),

                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    // color: Theme.of(context).primaryColor,
                    color: Color(0xFF00A0DC),
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
