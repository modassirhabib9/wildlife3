class Constants {
  static const String Setting = 'Setting';
  static const String Subscribe = 'Subscribe';
  static const String SignOut = 'Sign Out';

  static const Set<String> choices = <String>{
    Subscribe,
    Setting,
    SignOut,
  };
}
